#!/usr/bin/python

from datadog import initialize, api
import time
import json
import sys
import os
from pprint import pprint
import datetime
from pandas import Series
import matplotlib.pyplot as plt
from cycler import cycler


hosts = ["cenario1-m.c.ppca-209200.internal","cenario1-w-0.c.ppca-209200.internal","cenario1-w-1.c.ppca-209200.internal"]

papel = {
    "cenario1-m.c.ppca-209200.internal" : "m",
    "cenario1-w-0.c.ppca-209200.internal" : "s1",
    "cenario1-w-1.c.ppca-209200.internal" : "s2"
}

options = {
    'api_key': 'e474c8851f80b3aec31b3e8893bd0762',
    'app_key': '7c8735fec8c3e210b6a6f79167e311baae557d6b'
}

initialize(**options)

metricas = {
	"mem_used" : "system.mem.used",
    "mem_total" : "system.mem.total",
	"cpu_user" : "system.cpu.user",
	"cpu_system" : "system.cpu.system",
	"net_b_in" : "system.net.bytes_rcvd",
	"net_b_out" : "system.net.bytes_sent",
	"io_r" : "system.io.r_s",
    "io_w" : "system.io.w_s"
	}

titulo = {
	"system.mem.used" : "Uso de memoria",
	"system.cpu.user" : "Uso de CPU",
	"system.cpu.system" : "Uso de CPU",
	"system.net.bytes_rcvd" : "Bytes Transferidos",
	"system.net.bytes_sent" : "Bytes Transferidos",
	"system.io.r_s" : "IO por segundo",
    "system.io.w_s" : "IO por segundo"
}

memoria = {
    "cenario1" : 3891941376,
    "cenario2" : 3891941376,
    "cenario3" : 3891941376,
    "cenario4" : 7783882752,
    "cenario5" : 7783882752,
    "cenario6" : 7783882752,
    "cenario7" : 15567765504,
    "cenario8" : 15567765504,
    "cenario9" : 15567765504
}

cenario = "cenario1"

provider = "gcp"

start = 1531236052
end = 1531236362

metrica = metricas[str(sys.argv[1])]

if not os.path.exists("json"):
    os.mkdir("json",0755)

if not os.path.exists("csv"):
    os.mkdir("csv",0755)

for host in hosts:

    if metrica in (metricas["io_r"],metricas["io_w"]):
        query = 'sum:system.io.w_s{host:' + host + '}+sum:system.io.r_s{host:' + host + '}'
    elif metrica in (metricas["net_b_in"],metricas["net_b_out"]):
        query = 'sum:system.net.bytes_rcvd{host:' + host + '}+sum:system.net.bytes_sent{host:' + host + '}'
    else:
        query = 'avg:'+ metrica + '{host:' + host + '}'

    results = api.Metric.query(start=start, end=end, query=query)

    d = json.dumps(results)

    f = open('json/'+ query + '.json', 'w') #this will create a file name out.txt in the folder you are in
    print >> f, d
    f.close()

    l = json.loads(d)

    points = l["series"][0]["pointlist"]
    xs=[]
    ys=[]
    f1 = open('csv/csvdatas_'+ host + '_' + metrica + '.csv','w')
    f1.write("data;valor\n")
    f2 = open('csv/csvminutos_'+ host + '_' + metrica + '.csv','w')
    f2.write("minutos;valor\n")
    f3 = open('csv/csvbruto_'+ host + '_' + metrica +'.csv','w')
    f3.write("timestamp;valor\n")
    u=len(points)-1
    i=0
    for p in points:
        f1.write(datetime.datetime.fromtimestamp(p[0]/1000).strftime('%d/%m/%Y %H:%M:%S'))
        x=((points[u][0])-(points[u-i][0]))/1000
        xs.append(x)
        f2.write(str(x));
        f3.write(str(p[0]))
        f1.write(";")
        f2.write(";")
        f3.write(";")
        f1.write(str(p[1]).replace(".",","))
        f2.write(str(p[1]).replace(".",","))
        f3.write(str(p[1]).replace(".",","))
        if metrica == 'system.mem.used':
            ys.append((p[1])/memoria[cenario]*100)
        elif metrica in (metricas["net_b_in"],metricas["net_b_in"]):
            ys.append(p[1]/1024/1024)
        else:
            ys.append(p[1])
        f1.write('\n')
        f2.write('\n')
        f3.write('\n')
        i=i+1
    f1.close()
    f2.close()
    f3.close()

    #linestyle_cycler = cycler('linestyle',['-','--',':','-.'])
    #plt.rc('axes', prop_cycle=linestyle_cycler)
    #plt.plot(xs,ys,linewidth=2.0,c='r',label=host)
    if metrica in (metricas["net_b_in"],metricas["net_b_out"]):
         default_cycler = cycler('color', ['r', 'g', 'b', 'y']) + cycler('linestyle', ['-', ':', '-.', ':'])
    else:
        default_cycler = cycler('color', ['r', 'g', 'b', 'y', 'c', 'm', 'orange']) + cycler('linestyle', ['-', '-', '-', '-','-', '-', '-'])
    #plt.rc('lines', linewidth=4)
    plt.rc('axes', prop_cycle=default_cycler)
    plt.plot(xs,ys,linewidth=4.0,label=papel[host],alpha=0.7)


plt.title( titulo[metrica] + " " + cenario + " " + provider)

if metrica in (metricas["mem_used"]):
    plt.legend(loc='lower left')
else:
    plt.legend(loc='upper left')

plt.xlabel('Segundos')

if metrica in (metricas["mem_used"],metricas["cpu_user"]):
    plt.ylabel('%')
elif metrica in (metricas["net_b_in"],metricas["net_b_out"]):
    plt.ylabel('MB')
    plt.ylim(ymin=-1)
elif metrica in (metricas["io_r"],metricas["io_w"]):
    plt.ylabel('IOPS')

plt.savefig(cenario + '_' + provider + '_' + metrica + '.png')
#plt.show();
